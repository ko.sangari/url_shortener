'''tests for app.py functions'''
import unittest
from app import URLShortener


class TestFunctionality(unittest.TestCase):
    '''test each functionality aspects of the package'''
    def setUp(self):
        self.shortener = URLShortener('localhost', 6379, 0)

    def test_url_validation(self):
        '''
        check the url validation. URLs must be in complete format like 'https://www.google.com' .
        '''
        self.assertIsNone(self.shortener.generate('www.google.com'))
        self.assertIsNone(self.shortener.generate('ww.google.com'))
        self.assertIsNone(self.shortener.generate('https://www.google./'))
        self.assertIsNotNone(self.shortener.generate('https://www.google.com/'))

    def test_hash_function(self):
        '''
        check if the hash code is unique for the same url.
        '''
        self.assertEqual(self.shortener.create_hash('https://www.google.com/'), 'd75277cd')
        self.assertEqual(self.shortener.create_hash('https://www.google.com/'), 'd75277cd')
        self.assertEqual(self.shortener.create_hash('https://www.google.com/'), 'd75277cd')

    def test_shorter_than_original(self):
        '''
        check if the hashed url shorter than orginal one.
        '''
        for url in ['https://www.bing.com/', 'https://www.amazon.com/', 'https://www.alibaba.ir/']:
            self.assertLess(self.shortener.generate(url), url)

    def test_encode_decode_function(self):
        '''
        check if the orginal url is equal to the url which decoded from hashed one.
        '''
        site_list = [
            'https://www.google.com/',
            'https://www.yahoo.com/',
            'https://www.digikala.com/'
        ]
        for url in site_list:
            self.assertEqual(url, self.shortener.retrieve(self.shortener.generate(url)))


if __name__ == '__main__':
    unittest.main()
