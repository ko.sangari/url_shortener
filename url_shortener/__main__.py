'''This file manage WSGI server and handel url requests and responses.'''
import json
import datetime
import argparse
from wsgiref.simple_server import make_server
from .app import URLShortener


def generate_response(response, result, status):
    '''create a proportional response according to the given args.'''
    http_status = {
        200: "200 OK",
        400: "400 Bad Request",
        404: "404 Not Found",
    }
    message = 'error'
    if status in range(200, 300):
        message = 'result'
    status = http_status.get(status, None)
    headers = [('Content-type', 'application/json; charset=utf-8')]
    response(status, headers)
    return [json.dumps({message: result}).encode('utf-8')]


def web_app_shortener(environment, response):
    '''manage WSGI in shortener mode'''
    if environment['REQUEST_METHOD'] == 'POST' and '/api/v1/shortener' in environment['PATH_INFO']:
        if environment['CONTENT_LENGTH']:
            url = json.loads(environment['wsgi.input']\
                .read(int(environment['CONTENT_LENGTH'])))['url']
            result = SHORTENER.generate(url)
            if result:
                return generate_response(response, result, 200)
            return generate_response(response, 'Malformed URL.', 400)
    return generate_response(response, 'Please check your URL.', 404)


def web_app_redirect(environment, response):
    '''manage WSGI in redirect mode'''
    if environment['REQUEST_METHOD'] == 'GET' and environment['PATH_INFO'].count('/'):
        result = SHORTENER.retrieve(environment['PATH_INFO'].replace('/', ''))
        if result:
            return generate_response(response, result, 200)
    return generate_response(response, 'Please check your URL.', 404)


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='Process some integers.')
    PARSER.add_argument('--servermode', type=str, help='server mode: [shortenr/redirect]',\
        required=True)
    PARSER.add_argument('--redishost', type=str, help='redis host address')
    PARSER.add_argument('--redisport', type=int, help='redis port number')
    PARSER.add_argument('--redisdb', type=int, help='redis db number')
    ARGS = PARSER.parse_args()

    if ARGS.servermode == 'shortener':
        SERVERPORT = 8000
        with make_server('', SERVERPORT, web_app_shortener) as server:
            print(datetime.datetime.now().strftime('%B %d, %Y - %H:%M:%S'))
            print('Using Python3 & Redis')
            print(f'Starting development server at http://127.0.0.1:{SERVERPORT}/')
            print('Quit the server with CONTROL-C.')
            print('Shortener Mode ...')
            SHORTENER = URLShortener(host=ARGS.redishost, port=ARGS.redisport, db=ARGS.redisdb)
            try:
                server.serve_forever()
            except KeyboardInterrupt:
                print('\nBye bye!')

    elif ARGS.servermode == 'redirect':
        SERVERPORT = 8001
        with make_server('', SERVERPORT, web_app_redirect) as server:
            print(datetime.datetime.now().strftime('%B %d, %Y - %H:%M:%S'))
            print('Using Python3 & Redis')
            print(f'Starting development server at http://127.0.0.1:{SERVERPORT}/')
            print('Quit the server with CONTROL-C.')
            print('Redirect Mode ...')
            SHORTENER = URLShortener(host=ARGS.redishost, port=ARGS.redisport, db=ARGS.redisdb)
            try:
                server.serve_forever()
            except KeyboardInterrupt:
                print('\nBye bye!')

    else:
        raise Exception('unknown --servermode.')
