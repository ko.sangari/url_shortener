'''This file manage the main functionalities of the package.'''
import hashlib
import redis
from validators.url import url as url_validator


class URLShortener:
    '''main class of the package.'''
    def __init__(self, host, port, db):
        '''initialize the redis connection based on given specs.'''
        if host is None:
            host = 'localhost'
        if port is None:
            port = 6379
        if db is None:
            db = 0
        self.redis_server = redis.Redis(host=host, port=port, db=db)

    def generate(self, original_url):
        '''converts the orginal url to the shorten one, and then store it in redis.'''
        if not url_validator(original_url):
            return None

        hash_url = URLShortener.create_hash(original_url)
        if self.redis_server.get(hash_url):
            return hash_url

        self.redis_server.set(hash_url, original_url, ex=86400)
        return hash_url

    def retrieve(self, hashed):
        '''retrieve the orginal url from redis with given hashed url.'''
        if self.redis_server.get(hashed):
            return self.redis_server.get(hashed).decode('utf-8')
        return None

    @staticmethod
    def create_hash(original_url):
        '''generates a unique hash code for the given url.'''
        hash_object = hashlib.md5(original_url.encode('utf-8'))
        return hash_object.hexdigest()[:8]
