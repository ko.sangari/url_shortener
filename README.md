# URL Shortener package
As the package name suggests, this package is a service to help you to reduce the length of the URL so that it can be shared easily, and retrieve them when you need.


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install:

```bash
pip install url_shortener_package
```

## Package Instructions
This package runs in two separate mode, one is 'shortener' mode and another mode is 'redirect'.
> to proceed, you must choose the server mode.

> shortener mode runs on port 8000 and the redirect mode runs on port 8001.

e.g.:
```bash
python3 -m url_shortener --servermode shortener
python3 -m url_shortener --servermode redirect
```

Also you can set the redis connection server specs.
> these settings are optional and have standard default values.

> please note that you must have installed the 'redis-server' library on your system.
```bash
apt install redis-server
```
e.g.:

```bash
python3 -m url_shortener --servermode redirect --redishost localhost --redisport 6379 --redisdb 0
```

and for more information, run:

```bash
python3 -m url_shortener -h
```


### APIs Instruction

> all URLs must be in complete format, e.g.: https://www.google.com
* in shortener mode:
```bash
	method: POST
	api: /api/v1/shortener
	JSON body: {"url": <orginal_url>}
	response: {"result": <hashed-url>}
```

* in redirect mode:
```bash
	method: GET
	url: /<hashed-url>
	response: {"result": <orginal_url>}
```