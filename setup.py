import pathlib
import setuptools

HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()
setuptools.setup(
    name="url_shortener_package",
    version="0.0.3",
    description="a simple url shortener",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ko.sangari/url_shortener",
    author="Koosha Sangari",
    classifiers=[
        "Programming Language :: Python :: 3.6",
    ],
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=["redis", "validators"],
    python_requires='>=3.6'
)
